import apigateway = require('@aws-cdk/aws-apigateway');
import cdk = require('@aws-cdk/cdk');
import dynamodb = require('@aws-cdk/aws-dynamodb');
import iam = require('@aws-cdk/aws-iam');
import lambda = require('@aws-cdk/aws-lambda');

export class SaveRequestStack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const requestTable = new dynamodb.Table(this,
            'request-table',
            {
                tableName: 'request',
                partitionKey: {
                    name: 'id',
                    type: dynamodb.AttributeType.String
                },
                billingMode: dynamodb.BillingMode.PayPerRequest
            });

        const requestHandler = new lambda.Function(this,
            'request-handler',
            {
                runtime: lambda.Runtime.Python37,
                handler: 'app.lambda_handler',
                functionName: 'insert-request',
                environment: {
                    "REQUEST_TABLE": requestTable.tableName
                },
                description: "Request handler to insert a request into DynamoDB. Triggered by API Gateway.",
                code: lambda.Code.asset('./request-handler')
            });

        requestHandler.addToRolePolicy(new iam.PolicyStatement()
            .addAction("dynamodb:PutItem")
            .addResource(requestTable.tableArn)
        );

        const requestRestApi = new apigateway.LambdaRestApi(this,
            'request-api',
            {
                proxy: false,
                handler: requestHandler
            }
        );

        const requestRestApiSendResource = requestRestApi.root.addResource('send');

        const badRequestResponse: apigateway.IntegrationResponse = {statusCode: "400"};
        const internalServerResponse: apigateway.IntegrationResponse = {statusCode: "500"};
        const okResponse: apigateway.IntegrationResponse = {statusCode: "200"};

        const requestRestApiLambdaIntegration = new apigateway.LambdaIntegration(requestHandler, {
            integrationResponses: [badRequestResponse, internalServerResponse, okResponse],
        });

        requestRestApiSendResource.addMethod('POST', requestRestApiLambdaIntegration);
    }
}
