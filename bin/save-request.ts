#!/usr/bin/env node
import 'source-map-support/register';
import {SaveRequestStack} from '../lib/save-request-stack';
import cdk = require('@aws-cdk/cdk');

const app = new cdk.App();
new SaveRequestStack(app, 'SaveRequestStack');
app.run();
